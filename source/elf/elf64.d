module elf.elf64; 

private import elf.consts;

public{
    struct Dyn{
        long d_tag;
        ulong d_val;
    }
    struct Rel{
        ulong r_offset;
        ulong r_info;
    }
    struct Rela{
        ulong r_offset;
        ulong r_info;
        long r_addend;
    }
    struct Sym{
        uint st_name;
        byte st_info;
        byte st_other;
        ushort st_shndx;
        ulong st_value;
        ulong st_size;
        
        int type(){ return st_info & 0x0f; }
        int bind(){ return st_info >> 4; }
    }
    struct Ehdr{
        byte[EI.NIDENT] e_ident;
        ushort e_type;
        ushort e_machine;
        uint e_version;
        ulong e_entry;
        ulong e_phoff;
        ulong e_shoff;
        uint e_flags;
        ushort e_ehsize;
        ushort e_phentsize;
        ushort e_phnum;
        ushort e_shentsize;
        ushort e_shnum;
        ushort e_shstrndx;
    }
    struct Phdr{
        PT p_type;
        PF p_flags;
        ulong p_offset;
        ulong p_vaddr;
        ulong p_paddr;
        ulong p_filesz;
        ulong p_memsz;
        ulong p_align;
    }
    struct Shdr{
        uint sh_name;
        uint sh_type;
        ulong sh_flags;
        ulong sh_addr;
        ulong sh_offset;
        ulong sh_size;
        uint sh_link;
        uint sh_info;
        ulong sh_addralign;
        ulong sh_entsize;
    }
    struct Nhdr{
        uint n_namesz;
        uint n_descsz;
        uint n_type;
    }
}
