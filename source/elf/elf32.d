module elf.elf32; 

private import elf.consts;

public{
    struct Dyn{
        int d_tag;
        int d_val;
    }
    struct Rel{
        uint r_offset;
        uint r_info;
    }
    struct Rela{
        uint r_offset;
        uint r_info;
        int r_addend;
    }
    struct Sym{
        uint st_name;
        uint st_value;
        uint st_size;
        byte st_info;
        byte st_other;
        ushort st_shndx;
        
        int type(){ return st_info & 0x0f; }
        int bind(){ return st_info >> 4; }
    }
    struct Ehdr{
        byte[EI.NIDENT] e_ident;
        ushort e_type;
        ushort e_machine;
        uint e_version;
        uint e_entry;
        uint e_phoff;
        uint e_shoff;
        uint e_flags;
        ushort e_ehsize;
        ushort e_phentsize;
        ushort e_phnum;
        ushort e_shentsize;
        ushort e_shnum;
        ushort e_shstrndx;
    }
    struct Phdr{
        PT p_type;
        uint p_offset;
        uint p_vaddr;
        uint p_paddr;
        uint p_filesz;
        uint p_memsz;
        PF p_flags;
        uint p_align;
    }
    struct Shdr{
        uint sh_name;
        uint sh_type;
        uint sh_flags;
        uint sh_addr;
        uint sh_offset;
        uint sh_size;
        uint sh_link;
        uint sh_info;
        uint sh_addralign;
        uint sh_entsize;
    }
    struct Nhdr{
        uint n_namesz;
        uint n_descsz;
        uint n_type;
    }
}
