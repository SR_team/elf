module elf.consts;

enum PT : uint{
    NULL = 0,
    LOAD,
    DYNAMIC,
    INTERP,
    NOTE,
    SHLIB,
    PHDR,
    TLS,
    LOOS = 0x60000000,
    HIOS = 0x6fffffff,
    LOPROC = 0x70000000,
    HIPROC = 0x7fffffff,
    GNU_EH_FRAME = 0x6474e550,
    GNU_PROPERTY = 0x6474e553,
    GNU_STACK = 0x6474E551
}

enum PN{
    XNUM = 0xffff
}

enum ET{
    NONE = 0,       
    REL,
    EXEC,
    DYN,
    CORE,
    LOPROC = 0xff00,
    HIPROC = 0xffff
}

enum DT{
    NULL = 0,
    NEEDED,
    PLTRELSZ,
    PLTGOT,
    HASH,
    STRTAB,
    SYMTAB,
    RELA,
    RELASZ,
    RELAENT,
    STRSZ,
    SYMENT,
    INIT,
    FINI,
    SONAME,
    RPATH,
    SYMBOLIC,
    REL,
    RELSZ,
    RELENT,
    PLTREL,
    DEBUG,
    TEXTREL,
    JMPREL,
    ENCODING = 32,
    OLD_LOOS = 0x60000000,
    LOOS = 0x6000000d,
    HIOS = 0x6ffff000,
    VALRNGLO = 0x6ffffd00,
    VALRNGHI = 0x6ffffdff,
    ADDRRNGLO = 0x6ffffe00,
    ADDRRNGHI = 0x6ffffeff,
    VERSYM = 0x6ffffff0,
    RELACOUNT = 0x6ffffff9,
    RELCOUNT = 0x6ffffffa,
    FLAGS_1 = 0x6ffffffb,
    VERDEF = 0x6ffffffc,
    VERDEFNUM = 0x6ffffffd,
    VERNEED = 0x6ffffffe,
    VERNEEDNUM = 0x6fffffff,
    OLD_HIOS = 0x6fffffff,
    LOPROC = 0x70000000,
    HIPROC = 0x7fffffff,
}

enum STB{
    LOCAL = 0,
    GLOBAL,
    WEAK
}

enum STT{
    NOTYPE = 0,
    OBJECT,
    FUNC,
    SECTION,
    FILE,
    COMMON,
    TLS
}

enum PF : uint{
    R = 0x4,
    W = 0x2,
    X = 0x1,
    
    RW = 0x6,
    RX = 0x5,
    WX = 0x3,
    
    RWX = 0x7
}

enum SHT{
    NULL = 0,
    PROGBITS,
    SYMTAB,
    STRTAB,
    RELA,
    HASH,
    DYNAMIC,
    NOTE,
    NOBITS,
    REL,
    SHLIB,
    DYNSYM,
    NUM,
    LOPROC = 0x70000000,
    HIPROC = 0x7fffffff,
    LOUSER = 0x80000000,
    HIUSER = 0xffffffff
}

enum SHF{
    WRITE = 0x1,
    ALLOC = 0x2,
    EXECINSTR = 0x4,
    RELA_LIVEPATCH = 0x00100000,
    RO_AFTER_INIT = 0x00200000,
    MASKPROC = 0xf0000000
}

enum SHN{
    UNDEF = 0,
    LORESERVE = 0xff00,
    LOPROC = 0xff00,
    HIPROC = 0xff1f,
    LIVEPATCH = 0xff20,
    ABS = 0xfff1,
    COMMON = 0xfff2,
    HIRESERVE = 0xffff
}

enum EI{
    NIDENT = 16
}
